import sqlalchemy
from . import auth, flask_bcrypt
from .models import User
from flask import g


@auth.verify_password
def verify_password(username, password):
    """Verify a username/hashed password tuple."""
    
    try:
        user = User.query.filter_by(username=username).one()
    except sqlalchemy.orm.exc.NoResultFound:
        # We found no username that matched
        return False
        
    # Perform password hash comparison in time-constant manner.
    verified = flask_bcrypt.check_password_hash(user.password, password)

    if verified is True:
        g.current_user = user

    return verified
