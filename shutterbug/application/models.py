from application import db, flask_bcrypt
from sqlalchemy_hybrid import hybrid_property


import datetime


class User(db.Model):
    """SQLAlchemy User model."""

    # The primary key for each user record.
    id = db.Column(db.Integer, primary_key=True)
    
    # The unique email for each user record.
    email = db.Column(db.String(255), unique=True, nullable=False)
    
    # The unique username for each record.
    username = db.Column(db.String(40), unique=True, nullable=False)
    
    # The bcrypt'ed user password
    _password = db.Column('password', db.String(60), nullable=False)
    
    #  The date/time that the user account was created on.
    created_on = db.Column(db.DateTime,
        default=datetime.datetime.utcnow)
        
    def __repr__(self):
        return '<User %r>' % self.username
        

    @hybrid_property
    def password(self):
        """The bcrypt'ed password of the given user."""
        
        return self._password
        
    @password.setter
    def password(self, password):
        """Bcrypt the password on assignment."""
        
        self._password = flask_bcrypt.generate_password_hash(password)


class Photo(db.Model):
    """SQLAlchemy Photo model."""
    
    # The unique primary key for each photo created.
    id = db.Column(db.Integer, primary_key=True)
    
    # The free-form text-based comment of each photo.
    comment = db.Column(db.Text())
    
    # Path to photo on local disk
    path = db.Column(db.String(255), nullable=False)
    
    #  The date/time that the photo was created on.
    created_on = db.Column(db.DateTime(),
        default=datetime.datetime.utcnow, index=True)
        
    # The user ID that created this photo.
    user_id = db.Column(db.Integer(), db.ForeignKey('user.id'))
    
    # The attribute reference for accessing photos posted by this user.
    user = db.relationship('User', backref=db.backref('photos', lazy='dynamic'))

    def __repr__(self):
        return '<Photo %r>' % self.comment

