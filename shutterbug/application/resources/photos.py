from flask.ext.restful import Resource, reqparse, fields, marshal
photos_parser = reqparse.RequestParser()
photos_parser.add_argument('page', type=int, required=False, default=1, location='args')
photos_parser.add_argument('limit', type=int, required=False, default=10, location='args')



photo_fields = {
    'path': fields.String,
    'comment': fields.String,
    'created_on': fields.DateTime(dt_format='rfc822'),
}


class SinglePhoto(Resource):

    def get(self, photo_id):
        """Handling of GET requests."""
        pass
        
    def delete(self, photo_id):
        """Handling of DELETE requests."""
        pass


class ListPhoto(Resource):

    method_decorators = [auth.login_required]
    
    def get(self):
        """Get reverse chronological list of photos for the
            currently authenticated user."""
        data = photos_parser.parse_args(strict=True)
        offset = (data['page'] - 1) * data['limit']
        photos = g.current_user.photos.order_by(
                models.Photo.created_on.desc()).limit(
                data['limit']).offset(offset)

        return marshal(list(photos), photo_fields), 200
        
    def post(self):
        """Handling of POST requests."""
        pass
