from flask_restful import Resource, abort, reqparse, url_for
from flask import current_app, request, g, url_for
from application import auth, db, models
import uuid
import os
import werkzeug


new_user_parser = reqparse.RequestParser()
new_user_parser.add_argument('username', type=str, required=True)
new_user_parser.add_argument('email', type=str, required=True)
new_user_parser.add_argument('password', type=str, required=True)

new_photo_parser = reqparse.RequestParser()
new_photo_parser.add_argument('comment', type=str, required=False)
new_photo_parser.add_argument('photo', type=werkzeug.datastructures.FileStorage,
                              required=True, location='files')


class SingleUser(Resource):

    method_decorators = [auth.login_required]
    
    def get(self, user_id):
        """Handling of GET requests."""

        if g.current_user.id != user_id:
            # A user may only access their own user data.
            abort(403, message="You have insufficient permissions to access this resource.")

        # We could simply use the `current_user`,
        # but the SQLAlchemy identity map makes this a virtual
        # no-op and alos allows for future expansion
        # when users may access information of other users
        try:
            user = User.query.filter(User.id == user_id).one()
        except sqlalchemy.orm.exc.NoResultFound:
            abort(404, message="No such user exists!")

        data = dict(id=user.id,
                    username=user.username,
                    email=user.email,
                    created_on=user.created_on)
    
        return data, 200
        

class CreateUser(Resource):
    
    def post(self):
        """Handling of POST request."""

        data = new_user_parser.parse_args(strict=True)
        user = User(**data)

        db.session.add(user)
            
        try:
            db.session.commit()
        except sqlalchemy.exc.IntegrityError:
            abort(409, message="User already exists!")

        data = dict(id=user.id,
                    username=user.username,
                    email=user.email,
                    created_on=user.created_on)

        return data, 201, {'Location': url_for('singleuser', user_id=user.id, _external=True)}


class UploadPhoto(Resource):
    
    method_decorators = [auth.login_required]
    
    def post(self):
        """Adds a new photo via form-encoded POST data."""
        data = new_photo_parser.parse_args(strict=True)

        # Save our file to the filesystem first
        f = request.files['photo']

        extension = os.path.splitext(f.filename)[1]
        name = werkzeug.utils.secure_filename(str(uuid.uuid4()) + extension)
        path = os.path.join(current_app.config['UPLOAD_FOLDER'], name)

        f.save(path)

        data['user_id'] = g.current_user.id
        data['path'] = path

        # Get rid of the binary data that was sent; we've already
        # saved this to disk.
        del data['photo']
    
        # Add a new Photo entry to the database once we have
        # successfully saved the file to the filesystem above.
        photo = models.Photo(**data)
        db.session.add(photo)
        db.session.commit()
            
        data = dict(id=photo.id, path=photo.path,
                    comment=photo.comment,
                    created_on=photo.created_on)

        return data, 201, {'Location': url_for('singlephoto',
                                               photo_id=photo.id,
                                               _external=True)}
