import io
import base64
from application.models import User, Photo


def test_unauthenticated_form_upload_of_simulated_file(session, client):
    """Ensure that we can't upload a file via un-authed form POST."""

    data = dict(file=(io.BytesIO(b'A test file.'), 'test.png'))
        
    response = client.post('/photos', data=data)
    assert response.status_code == 401


def test_authenticated_form_upload_of_simulated_file(session, client):
    """Upload photo via POST data with authenticated user."""

    password = 'foobar'
    user = User(username='you', email='you@example.com', password=password)

    session.add(user)

    data = dict(photo=(io.BytesIO(b'A test file.'), 'test.png'))

    creds = base64.b64encode(b'{0}:{1}'.format(user.username, password)).decode('utf-8')
    
    response = client.post('/photos', data=data, headers={'Authorization': 'Basic ' + creds})

    assert response.status_code == 201
    assert 'Location' in response.headers

    photos = Photo.query.all()
    assert len(photos) == 1

    assert ('/photos/{}'.format(photos[0].id) in response.headers['Location'])


def test_upload_photo_with_comment(session, client):
    """Adds a photo with a comment."""

    password = 'foobar'
    user = User(username='you', email='you@example.com', password=password)

    session.add(user)
    
    data = dict(photo=(io.BytesIO(b'A photo with a comment.'),
                'new_photo.png'),
                comment='What an inspiring photo!')
        

    creds = base64.b64encode(b'{0}:{1}'.format(user.username, password)).decode('utf-8')
        
    response = client.post('/photos', data=data,
    headers={'Authorization': 'Basic ' + creds})

    assert response.status_code == 201
    assert 'Location' in response.headers

    photos = Photo.query.all()
    assert len(photos) == 1

    photo = photos[0]
    assert photo.comment == data['comment']
