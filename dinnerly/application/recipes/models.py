from application.users.services import twitter, facebook
from sqlalchemy import event
    
@event.listens_for(Recipe, 'after_insert')
def listen_for_recipe_insert(mapper, connection, target):
    """Listens for after_insert event from SQLAlchemy
    for Recipe model instances."""

    summary = target.summarize()
        
    if target.user.twitter:
        twitter_response = twitter.post('statuses/update.json', data={'status': summary})
            
        if twitter_response.status != 200:
            raise ValueError("Could not publish to Twitter.")
            
    if target.user.facebook:
        fb_response = facebook.post('/me/feed', data={
            'message': summary
        })
        if fb_response.status != 200:
            raise ValueError("Could not publish to Facebook.")
