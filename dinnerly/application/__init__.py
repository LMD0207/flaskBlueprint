from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_oauthlib.client import OAuth
from flask_login import LoginManager
from flask_alembic import Alembic


# Intialize the Alembic extension
alembic = Alembic()


# Deferred initialization of the db extension
db = SQLAlchemy()
oauth = OAuth()
login_manager = LoginManager()


def create_app(config=None):
    app = Flask(__name__, static_folder=None)
        
    if config is not None:
        app.config.from_object(config)
           
    import application.users.models
    import application.recipes.models
    #...
    alembic.init_app(app)

    db.init_app(app)
    oauth.init_app(app)
    login_manager.init_app(app)


    from application.users.views import users
    app.register_blueprint(users, url_prefix='/users')

    return app
