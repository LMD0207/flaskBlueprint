from flask import Blueprint, redirect, url_for, request, abort, render_template
from flask_login import login_user, current_user
from application.users.models import (User, TwitterConnection, FacebookConnection)
from application import oauth, db, login_manager
import sqlalchemy


users = Blueprint('users', __name__, template_folder='templates')


twitter = oauth.remote_app('twitter',
                            consumer_key='<consumer key>',
                            consumer_secret='<consumer secret>',
                            base_url='https://api.twitter.com/1.1/',
                            request_token_url='https://api.twitter.com/oauth/request_token',
                            access_token_url='https://api.twitter.com/oauth/access_token',
                            authorize_url='https://api.twitter.com/oauth/authenticate')


facebook = oauth.remote_app(
                            'facebook',
                            consumer_key='<facebook app id>',
                            consumer_secret='<facebook app secret>',
                            request_token_params={'scope': 'email,publish_actions'},
                            base_url='https://graph.facebook.com',
                            request_token_url=None,
                            access_token_url='/oauth/access_token',
                            access_token_method='GET',
                            authorize_url='https://www.facebook.com/dialog/oauth')

@login_manager.user_loader
    def load_user(user_id):
        return User.query.get(int(user_id))


@twitter.tokengetter
def get_twitter_token():
    """Fetch Twitter token from currently logged
    in user."""
    if (current_user.is_authenticated() and current_user.twitter):
        return (current_user.twitter.oauth_token, current_user.twitter.oauth_token_secret)
    return None

@facebook.tokengetter
def get_facebook_token():
    """Fetch Facebook token from currently logged
    in user."""
    if (current_user.is_authenticated() and current_user.facebook):
        return (current_user.facebook.oauth_token, )
    return None


@users.route('/login/twitter')
def login_twitter():
    """Kick-off the Twitter authorization flow if
    not currently authenticated."""
      
    if current_user.is_authenticated():
        return redirect(url_for('recipes.index'))
    return twitter.authorize(callback=url_for('.twitter_authorized', _external=True))


@users.route('/login/twitter-authorized')
def twitter_authorized():
    resp = twitter.authorized_response()
      
    try:
        user = db.session.query(User).join(
                                           TwitterConnection).filter(
                                            TwitterConnection.oauth_token == resp['oauth_token']).one()
    except sqlalchemy.orm.exc.NoResultFound:
        credential = TwitterConnection(twitter_user_id=int(resp['user_id']),
                        screen_name=resp['screen_name'],
                        oauth_token=resp['oauth_token'],
                        oauth_token_secret=resp['oauth_token_secret'])
        user = User(username=resp['screen_name'])
        user.twitter = credential

        db.session.add(user)
        db.session.commit()
        db.session.refresh(user)

    login_user(user)
    return redirect(url_for('recipes.index'))


@users.route('/twitter/find-friends')
@login_required
def twitter_find_friends():
    """Find common friends."""
    
    if not current_user.twitter:
        abort(403)

    twitter_user_id = current_user.twitter.twitter_user_id

    # This will only query 5000 Twitter user IDs.
    # If your users have more friends than that,
    # you will need to handle the returned cursor
    # values to iterate over all of them.
    response = twitter.get('friends/ids?user_id={}'.format(twitter_user_id))

    friends = response.json().get('ids', list())
    friends = [int(f) for f in friends]

    common_friends = User.query.filter(User.twitter_user_id.in_(friends))

    return render_template('users/friends.html', friends=common_friends)



@users.route('/login/facebook')
def login_facebook():
    """Kick-off the Facebook authorization flow if
    not currently authenticated."""
    if current_user.is_authenticated():
        return redirect(url_for('recipes.index'))
    return facebook.authorize(callback=url_for('.facebook_authorized', _external=True))


@users.route('/login/facebook-authorized')
def facebook_authorized():
    """Handle the authorization grant & save the token."""

    resp = facebook.authorized_response()
    me = facebook.get('/me')
    
    try:
        user = db.session.query(User).join(
                FacebookConnection).filter(
                TwitterConnection.oauth_token == resp['access_token']).one()
    except sqlalchemy.orm.exc.NoResultFound:
        credential = FacebookConnection(name=me.data['name'],facebook_id=me.data['id'], access_token=resp['access_token'])

        user = User(username=resp['screen_name'])
        user.twitter = credential

        db.session.add(user)
        db.session.commit()
        db.session.refresh(user)

    login_user(user)
    return redirect(url_for('recipes.index'))

