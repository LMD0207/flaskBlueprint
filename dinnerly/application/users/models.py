import datetime
from application import db
    
class User(db.Model):
    
    # The primary key for each user record.
    id = db.Column(db.Integer, primary_key=True)
   
    # The username for a user. Might not be
    username = db.Column(db.String(40))
    
    #  The date/time that the user account was created on.
    created_on = db.Column(db.DateTime, default=datetime.datetime.utcnow)

    def __repr__(self):
        return '<User {!r}>'.format(self.username)


    def is_authenticated(self):
        """All our registered users are authenticated."""
        return True
         
    def is_active(self):
        """All our users are active."""
        return True
        
    def is_anonymous(self):
        """All users are not in an anonymous state."""
        return False
        
    def get_id(self):
        """Get the user ID as a Unicode string."""
        return unicode(self.id)

    twitter = db.relationship("TwitterConnection", uselist=False, backref="user")
    facebook = db.relationship("FacebookConnection", uselist=False, backref="user")


class TwitterConnection(db.Model):
    # The primary key for each connection record.
    id = db.Column(db.Integer, primary_key=True)

    # Our relationship to the User that this
    # connection belongs to.
    user_id = db.Column(db.Integer(), db.ForeignKey('user.id'), nullable=False, unique=True)

    # The twitter screen name of the connected account.
    screen_name = db.Column(db.String(), nullable=False)

    # The Twitter ID of the connected account
    twitter_user_id = db.Column(db.Integer(), nullable=False)
    
    # The OAuth token
    oauth_token = db.Column(db.String(), nullable=False)
    
    # The OAuth token secret
    oauth_token_secret = db.Column(db.String(), nullable=False)


class FacebookConnection(db.Model):
    
    # The primary key for each connection record.
    id = db.Column(db.Integer, primary_key=True)
    
    # Our relationship to the User that this
    # connection belongs to.
    user_id = db.Column(db.Integer(),
                        db.ForeignKey('user.id'), nullable=False)
        
    # The numeric Facebook ID of the user that this
    # connection belongs to.
    facebook_id = db.Column(db.Integer(), nullable=False)
       
    # The OAuth token
    access_token = db.Column(db.String(), nullable=False)
        
    # The name of the user on Facebook that this
    # connection belongs to.
    name = db.Column(db.String())


class Recipe(db.Model):
    # The unique primary key for each recipe created.
    id = db.Column(db.Integer, primary_key=True)
    
    # The title of the recipe.
    title = db.Column(db.String())

    # The ingredients for the recipe.
    # For the sake of simplicity, we'll assume ingredients
    # are in a comma-separated string.
    ingredients = db.Column(db.Text())
    
    # The instructions for each recipe.
    instructions = db.Column(db.Text())
    
    #  The date/time that the post was created on.
    created_on = db.Column(db.DateTime(),
                           default=datetime.datetime.utcnow,
                           index=True)
        
    # The user ID that created this recipe.
    user_id = db.Column(db.Integer(), db.ForeignKey('user.id'))
        
    # User-Recipe is a one-to-many relationship.
    user = db.relationship('User', backref=db.backref('recipes'))

    def summarize(self, character_count=136):
        """
        Generate a summary for posting to social media.
        """
        if len(self.title) <= character_count:
            return self.title

        short = self.title[:character_count].rsplit(' ', 1)[0]
        return short + '...'


