from application import oauth

twitter = oauth.remote_app('twitter', consumer_key='<consumer key>',
                           consumer_secret='<consumer secret>',
                           base_url='https://api.twitter.com/1/',
                           request_token_url='https://api.twitter.com/oauth/request_token',
                           access_token_url='https://api.twitter.com/oauth/access_token',
                           authorize_url='https://api.twitter.com/oauth/authenticate',
                           access_token_method='GET')


facebook = oauth.remote_app(
                            'facebook',
                            consumer_key='<consumer key>',
                            consumer_secret='<consumer secret>',
                            request_token_params={'scope': 'email,publish_actions'},
                            base_url='https://graph.facebook.com',
                            request_token_url=None,
                            access_token_url='/oauth/access_token',
                            access_token_method='GET',
                            authorize_url='https://www.facebook.com/dialog/oauth')
