from flask_script import Manager, prompt_bool, Server, Shell
from application import create_app, db, repositories
from application.repositories.cli import repository_manager

    
# Create the `manager` object with a
# callable that returns a Flask application object.
manager = Manager(app=create_app)


@manager.command
def init_db():
    """Initialize SQLAlchemy database models."""
    
    db.create_all()

def drop_db():
    if prompt_bool(
        "Are you sure you want to lose all your data"):
        db.drop_all()

def _context():
    """Adds additional objects to our default shell context."""
    return dict(db=db, repositories=repositories)


if __name__ == '__main__':
    manager.add_command('runserver', Server(port=6000))
    manager.add_command('shell', Shell(make_context=_context))
    manager.add_command('repositories', repository_manager)
    manager.run()

