from application import db
from sqlalchemy.schema import UniqueConstraint
import datetime


class Repository(db.Model):
    """Holds the meta-information about a particular Github repository."""
    # The unique primary key for the local repository record.
    id = db.Column(db.Integer, primary_key=True)
           
    # The name of the repository.
    name = db.Column(db.String(length=255), nullable=False)
       
    # The github org/user that owns the repository.
    owner = db.Column(db.String(length=255), nullable=False)
       
    # The description (if any) of the repository.
    description = db.Column(db.Text())
           
    #  The date/time that the record was created on.
    created_on = db.Column(db.DateTime(), default=datetime.datetime.utcnow, index=True)
           
    # The SQLAlchemy relation for the issues contained within this repository.
    issues = db.relationship('Issue')

    __table_args__ = (UniqueConstraint('name', 'owner'), )

    def __repr__(self):
        return u'<Repository {}>'.format(self.name)


class Issue(db.Model):
    """Holds the meta information regarding an issue that belongs to a repository."""
    # The autoincremented ID of the issue.
    id = db.Column(db.String(length=40), primary_key=True)
    # The repository ID that this issue belongs to.
    
    #
    # This relationship will produce a `repository` field
    # that will link back to the parent repository.
    repository_id = db.Column(db.Integer(), db.ForeignKey('repository.id'))
        
    # The title of the issue
    title = db.Column(db.String(length=255), nullable=False)
    
    # The issue number
    number = db.Column(db.Integer(), nullable=False)
    
    state = db.Column(db.Enum('open', 'closed'), nullable=False)
        
    def __repr__(self):
        """Representation of this issue by number."""
        return '<Issue {}>'.format(self.number)
