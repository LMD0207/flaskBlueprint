from github3 import login


class Githubber(object):
    """
    A Flask extension that wraps necessary configuration
    and functionality for interacting with the Github API
    via the `github3.py` 3rd party library.
    """
     
    def __init__(self, app=None):
        self._client = None
       """
        Initialize the extension.
        Any default configurations that do not require the application instance should be put here.
        """

        if app:
            self.init_app(app)
            

    def init_app(self, app):
        """ Initialize the extension with any application-level Configuration requirements. """
        if not hasattr(app, 'extensions'):
            app.extensions = {}
        
        if app.config.get('GITHUB_USERNAME') is None:
            raise ValueError("Cannot use Githubber extension without specifying the GITHUB_USERNAME.")

        if app.config.get('GITHUB_PASSWORD') is None:
            raise ValueError("Cannot use Githubber extension without specifying the GITHUB_PASSWORD.")

        # Store the state of the currently configured extension in
        # `app.extensions`.

        app.extensions['githubber'] = self
        self.app = app


    @property
    def client(self):
        if self._client:
            return self._client
    
        gh_client = login(self.app.config['GITHUB_USERNAME'],
                          password=self.app.config['GITHUB_PASSWORD'])

        self._client = gh_client
        return self._client
