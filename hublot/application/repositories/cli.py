from flask_script import Manager
from urllib.parse import urlparse
from application.repositories.models import Repository
from application import db, hubber
import sqlalchemy


repository_manager = Manager(usage="Repository-based CLI actions.")


@repository_manager.command
def add(url, description=None):
    """Adds a repository to our database."""

    parsed = urlparse(url)
    
    # Ensure that our repository is hosted on github
    if parsed.netloc != 'github.com':
        print("Not from Github! Aborting.")
        return(1)
    try:
        _, owner, repo_name = parsed.path.split('/')
    except ValueError:
        print("Invalid Github project URL format!")
        return(1)

    repository = Repository(name=repo_name, owner=owner)
    db.session.add(repository)

    try:
        db.session.commit()
    except sqlalchemy.exc.IntegrityError:
        print("That repository already exists!")
        return 1
        
    print("Created new Repository with ID: %d" % repository.id)
    return(0)


def fetch_issues(repository_id):
    """Fetch all commits for the given Repository."""
    
    try:
        repo = Repository.query.get(repository_id)
    except sqlalchemy.orm.exc.NoResultFound:
        print "No such repository ID!"
        return(1)

    r = hubber.client.repository(repo.owner, repo.name)
    issues = []

    for issue in r.iter_issues():
        i = Issue(repository_id=repo.id, title=issue.title,
                  number=issue.number, state=issue.state)

        issues.append(i)
    db.session.add_all(issues)
        print("Added {} issues!".format(len(issues)))
