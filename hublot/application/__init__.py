from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from application.extensions import Githubber


# Initialize the db extension, but without configuring
# it with an application instance.
db = SQLAlchemy()
hubber = Githubber()


def create_app(config=None):
    app = Flask(__name__)
    
    if config is not None:
        app.config.from_object(config)
        
    # Initialize extensions
    db.init_app(app)
    hubber.init_app(app)
    
    return app
