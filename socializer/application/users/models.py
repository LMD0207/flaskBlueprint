import datetime
from application import db, flask_bcrypt
from sqlalchemy_hybrid import hybrid_property

__all__ = ['followers', 'User']

# We use the explicit SQLAlchemy mappers for declaring the
# followers table, since it does not require any of the features

# that the declarative base model brings to the table.
#
# The `follower_id` is the entry that represents a user who
# *follows* a `user_id`.
followers = db.Table(
                     'followers',
                     db.Column('follower_id', db.Integer, db.ForeignKey('user.id'),
                               primary_key=True),
                     db.Column('user_id', db.Integer, db.ForeignKey('user.id'),
                               primary_key=True))


class User(db.Model):
    
    # The primary key for each user record.
    id = db.Column(db.Integer, primary_key=True)
    
    # The unique email for each user record.
    email = db.Column(db.String(255), unique=True)
    
    # The unique username for each record.
    username = db.Column(db.String(40), unique=True)
    
    # The hashed password for the user
    _password = db.Column('password', db.String(60))
    
    #  The date/time that the user account was created on.
    created_on = db.Column(db.DateTime,
                           default=datetime.datetime.utcnow)
    followed = db.relationship('User',
                                secondary=followers,
                                primaryjoin=(id==followers.c.follower_id ),
                                secondaryjoin=(id==followers.c.user_id),
                                backref=db.backref('followers', lazy='dynamic'),
                                lazy='dynamic')
        
    @hybrid_property
    def password(self):
        """The bcrypt'ed password of the given user."""
        return self._password


    @password.setter
    def password(self, password):
        """Bcrypt the password on assignment."""
        self._password = flask_bcrypt.generate_password_hash(password)


    def __repr__(self):
        return '<User %r>' % self.username


    def is_authenticated(self):
        """All our registered users are authenticated."""
        return True


    def is_active(self):
        """All our users are active."""
        return True


    def is_anonymous(self):
        """We don't have anonymous users; always False"""
        return False


    def get_id(self):
        """Get the user ID."""
        return unicode(self.id)









