from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt


# Initialize the db extension, but without configuring

# it with an application instance.
db = SQLAlchemy()
# The same for the Bcrypt extension
flask_bcrypt = Bcrypt()

def create_app(config=None):
    app = Flask(__name__)
    
    if config is not None:
        app.config.from_object(config)
    
    # Initialize any extensions and bind blueprints to the
    # application instance here.
    db.init_app(app)
    flask_bcrypt.init_app(app)

    return app
