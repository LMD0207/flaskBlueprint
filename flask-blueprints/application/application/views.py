from application import app

from flask import Blueprint


users = Blueprint('users', __name__)


@app.route("/")
def hello():
    return('Hello World!')


@app.route("/contact")
def contact():
    return("You can contact me at 555-5555, or "
    " email me at test@example.com")


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        # Logic for handling login
        pass
    else:
        # Display login form
        pass


@users.route('/me')
def me():
    return('This is my page.', 200)
